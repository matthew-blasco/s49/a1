import {Fragment, useState} from 'react';
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";


import Home from './pages/Home'
import Courses from './pages/Courses'
import Register from './pages/Register'
import Login from './pages/Login'
import Error from './pages/Error'
import Logout from './pages/Logout'
import AppNavbar from './components/AppNavbar'
import Footer from './components/Footer' 
import {UserProvider} from './UserContext';

function App() {

  const [user, setUser] = useState({
    id: 'hello',
    isAdmin: true
  });



  return(

    <UserProvider value={{user, setUser}}>
    <BrowserRouter>
      <AppNavbar/>
      <Routes>
        <Route path="/" element={ <Home/> } />
        <Route path="/courses" element={ <Courses/> } />
        <Route path="/register" element={ <Register/> } />
        <Route path="/login" element={ <Login/> } />
        <Route path="/logout" element={<Logout/>}/>
        <Route path="*" element={<Error/>}/>
      </Routes>
      <Footer/>
    </BrowserRouter>
    </UserProvider>

  )
}

export default App;
