
import coursesData from './../mockData/courses';
import CourseCards from './../components/CourseCards';
import {useContext} from 'react';
import UserContext from './../UserContext';

export default function Course(){

	const {user, setUser} = useContext(UserContext);
	console.log(user);

	const courses = coursesData.map(course => {
		return <CourseCards key={course.id} courseProp={course}/>
	})
	
	return(
		courses
	)
}
