import React, {Fragment} from 'react'
import Banner from "../components/Banner";


export default function Error(){

  const data = {
    title: "Error 404",
    description: "Page Not Found",
    destination: "/",
    buttonDesc: "Go back Home"
  }

  return <Banner bannerProp={data}/>

}
