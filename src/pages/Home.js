import {Fragment} from 'react';
import Banner from './../components/Banner';
import CourseCards from './../components/CourseCards';
import Course from './../components/Course';

export default function Home(){

	const data = {
		title: "Welcome to Course Booking",
		description: "Opportunities for everyone, everywhere",
		destination: "/courses",
		buttonDesc: "Check the courses"
	}

	return(
		//render navbar, banner & footer in the webpage via home
		<Fragment>
			<Banner bannerProp={data}/>
			<Course/>
		</Fragment>
	)
}
