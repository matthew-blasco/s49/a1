import {Form, Button, Row, Col, Container} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import {useNavigate} from 'react-router-dom'
import { useContext } from 'react';
import UserContext from './../UserContext';

export default function Register(){
	const [fN, setFn] = useState("");
	const [lN, setLn] = useState("");
	const [email, setEmail] = useState("");
	const [pw, setPw] = useState("");
	const [vPW, setVPW] = useState("");
	const [isDisabled, setIsDisabled] = useState(true);

	const {user, setUser} = useContext(UserContext);

	const navigate = useNavigate();


	// useEffect(function, options)
	//disable button if all fields not filled out
	useEffect(()=>{
		//console.log('render');

		//if all input fields are empty, keep the state of the button to true
		//if all fields are filled out, change the state to false

		//mini-activity my answer
		// if (fN!=="" && lN!=="" && email!=="" && pw!=="" && vPW!==""){
		// 	setIsDisabled(false);
		// } else {
		// 	setIsDisabled(true);
		// }

		//miniactivty soution
		if((fN !== "" && lN!=="" && email !== "" && pw !== "" && vPW !== "") && (pw==vPW)){
			setIsDisabled(false)
		} else {
			setIsDisabled(true)
		}

		if (user.isAdmin == true){
			navigate('/courses');
		}

		//listen to state changes, fn, ln, em, pw, vpw
	},[fN,lN,email,pw,vPW])

	const registerUser = (e) =>{
		e.preventDefault();
		//console.log(e);

		fetch('https://sleepy-gorge-55333.herokuapp.com/api/users/email-exists', {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(response => response.json())
		.then(response => {
			//console.log(response)
			if(!response){
				//send requestto register

				fetch('https://sleepy-gorge-55333.herokuapp.com/api/users/register', {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstName: fN,
						lastName: lN,
						email: email,
						password: pw
					})
				})
				.then(response => response.json())
				.then(response => {
					//console.log(response);

					if(response){
						alert('Registration successful.')
						//redirect
						// window.location.href('./login.html')
						navigate('/login');
					} else {
						alert('Something went wrong. Please try again.')
					}
				})

			} else {
				alert(`Email already registered.`)
			}
		})
	}

	return(
		<Container className="m-5">
			<h3 className="text-center">Register</h3>
			<Row className="justify-content-center">
				<Col xs={12} md={6}>
					<Form onSubmit={(e) => registerUser(e)}>
					 	<Form.Group className="mb-3">
					    	<Form.Label>First Name</Form.Label>
					    	<Form.Control type="text" value={fN}
					    	onChange={(e)=> setFn(e.target.value)}/>
					  	</Form.Group>

					  	<Form.Group className="mb-3">
					  	   	<Form.Label>Last Name</Form.Label>
					  	   	<Form.Control type="text" value={lN} onChange={(e)=> setLn(e.target.value)}/>
					  	</Form.Group>
					  	 <Form.Group className="mb-3">
					  	    <Form.Label>Email Address:</Form.Label>
					  	    <Form.Control type="email" value={email} onChange={(e)=> setEmail(e.target.value)}/>
					  	 </Form.Group>

					  <Form.Group className="mb-3">
					  		<Form.Label>Password</Form.Label>
					    	<Form.Control type="password" placeholder="Password" value={pw} onChange={(e)=> setPw(e.target.value)}/>
					  </Form.Group>

					  <Form.Group className="mb-3">
					  		<Form.Label>Verify Password</Form.Label>
					    	<Form.Control type="password" placeholder="Password" value={vPW} onChange={(e)=> setVPW(e.target.value)}/>
					  </Form.Group>
					  
					  <Form.Group className="mb-3">
					  		<Form.Check type="checkbox" label="Check me out" />
					  </Form.Group>
					  
					  <Button 
					  	variant="info" 
					  	type="submit"
					  	disabled={isDisabled}
					  >
					    Submit
					  </Button>
					</Form>
				</Col>
			</Row>
		</Container>
	)
}
